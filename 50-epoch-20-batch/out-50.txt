Script started on Wed Jun 20 13:09:56 2018
[01;32mbintang@bintang-gpu-2[00m:[01;34m~/notebooks/bintang-notebook/tugas-akhir/notebook-pydata[00m$ exitrunipy 10_epoch.ipynb out-50.ipynb
06/20/2018 01:10:03 PM INFO: Reading notebook 10_epoch.ipynb
06/20/2018 01:10:04 PM INFO: Running cell:
# RUN CELL 1
import keras                                                                    
import pandas as pd                                                             
from glob import glob                                                           
import os                                                                       
import cv2                                                                      
import numpy as np                                                              
from collections import Counter                                                 
import matplotlib.pyplot as plt                                                 
%matplotlib inline  


06/20/2018 01:10:06 PM INFO: Cell returned
06/20/2018 01:10:06 PM INFO: Running cell:
preprocess_path = "../"                                               
train_files = glob(os.path.join(preprocess_path, "preprocess/", "*.jpeg"))                      
                                                                                  
df = pd.read_csv("../trainLabels.csv") 

06/20/2018 01:10:06 PM INFO: Cell returned
06/20/2018 01:10:06 PM INFO: Running cell:
# RUN CELL 2
img_height = 299
img_width = 299
from imgaug import augmenters as iaa

seq = iaa.Sequential(
    [
        iaa.Fliplr(0.2), # horizontally flip 50% of all images
        iaa.Affine(
            scale=(0.9, 1.1),
            rotate=(-180, 180), # rotate by -180 to +180 degrees
        ),
    ],
    random_order=True
)

06/20/2018 01:10:06 PM INFO: Cell returned
06/20/2018 01:10:06 PM INFO: Running cell:
def get_level(file, df):
    basename = os.path.basename(file)
    image_id = basename.split(".")[0]

    mini_df = df[df['image'] == image_id]
    if len(mini_df) < 1:
        return None

    return mini_df.values[0][1]

06/20/2018 01:10:06 PM INFO: Cell returned
06/20/2018 01:10:06 PM INFO: Running cell:
# Example: 1 -> [0, 1, 0, 0, 0]
def get_onehot(level):
    level_vec = np.zeros(5)
    level_vec[level] = 1
    
    return level_vec
from keras.utils.data_utils import Sequence

06/20/2018 01:10:06 PM INFO: Cell returned
06/20/2018 01:10:06 PM INFO: Running cell:
class DataSequence(Sequence):
    def __init__(self, file_list, batch_size, augment=False):
        self.file_list = file_list
        self.batch_size = batch_size
        self.augment = augment

    def __len__(self):
        return len(self.file_list) // self.batch_size

    def __getitem__(self, idx):
        imgs = []
        labels = []
        
        i = idx * self.batch_size
        
        while len(imgs) < self.batch_size:
            file = self.file_list[i]
            
            level = get_level(file, df)
            if level is None:
                print("missing level: " + file)
                i += 1
                continue
            
            img = cv2.imread(file)
            img = cv2.resize(img,(img_height,img_width))
            
            if img is not None:
                imgs.append(img)
                labels.append(get_onehot(level))
                #class_weights = get_class_weights(current_epoch)

            i += 1

        # Image augmentation
        if self.augment:
            imgs = seq.augment_images(imgs)

        return np.asarray(imgs).astype(np.float16), np.asarray(labels).astype(np.int8)

06/20/2018 01:10:06 PM INFO: Cell returned
06/20/2018 01:10:06 PM INFO: Running cell:
# RUN CELL 3
from keras.models import Model
from keras.optimizers import Adam
from keras.applications.inception_v3 import InceptionV3
from keras.layers import Dense, Input, Flatten, Dropout, GlobalAveragePooling2D
from keras.layers.normalization import BatchNormalization
from keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau

img_channels = 3
img_dim = (img_height, img_width, img_channels)
from datetime import datetime as dt

06/20/2018 01:10:06 PM INFO: Cell returned
06/20/2018 01:10:06 PM INFO: Running cell:
def get_experiment_id():
    time_str = dt.now().strftime('%Y-%m-%d-%H-%M-%S')
    experiment_id = 'inceptin_{}'.format(time_str)
    return experiment_id

06/20/2018 01:10:06 PM INFO: Cell returned
06/20/2018 01:10:06 PM INFO: Running cell:
def inceptionv3(img_dim=img_dim):
    input_tensor = Input(shape=img_dim)
    base_model = InceptionV3(include_top=False,
                   weights='imagenet',
                   input_shape=img_dim)
    bn = BatchNormalization()(input_tensor)
    x = base_model(bn)
    x = GlobalAveragePooling2D()(x)
    x = Dropout(0.5)(x)
    output = Dense(5, activation='softmax')(x)
    model = Model(input_tensor, output)
    return model

06/20/2018 01:10:06 PM INFO: Cell returned
06/20/2018 01:10:06 PM INFO: Running cell:
model = inceptionv3()
model.summary()

2018-06-20 13:10:06.841726: I tensorflow/core/platform/cpu_feature_guard.cc:140] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
2018-06-20 13:10:06.964582: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1212] Found device 0 with properties: 
name: Tesla M60 major: 5 minor: 2 memoryClockRate(GHz): 1.1775
pciBusID: 8ed7:00:00.0
totalMemory: 7.94GiB freeMemory: 7.58GiB
2018-06-20 13:10:06.964632: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1312] Adding visible gpu devices: 0
2018-06-20 13:10:07.317539: I tensorflow/core/common_runtime/gpu/gpu_device.cc:993] Creating TensorFlow device (/job:localhost/replica:0/task:0/device:GPU:0 with 7322 MB memory) -> physical GPU (device: 0, name: Tesla M60, pci bus id: 8ed7:00:00.0, compute capability: 5.2)
06/20/2018 01:10:28 PM INFO: Cell returned
06/20/2018 01:10:28 PM INFO: Running cell:
# RUN CELL 4
train_files = glob(os.path.join(preprocess_path,"preprocess/" "*.jpeg"))
n_val_files = len(train_files) // 10
val_file_list = train_files[:n_val_files]
train_file_list = train_files[n_val_files:]

06/20/2018 01:10:28 PM INFO: Cell returned
06/20/2018 01:10:28 PM INFO: Running cell:
import os
os.environ["CUDA_VISIBLE_DEVICES"]="0"

06/20/2018 01:10:28 PM INFO: Cell returned
06/20/2018 01:10:28 PM INFO: Running cell:
import datetime
def get_experiment_id():
    time_str = datetime.datetime.now().strftime('%b-%d-%y-%H:%M:%S')
    experiment_id = 'inceptionV3_{}'.format(time_str)

    return experiment_id
get_experiment_id()

06/20/2018 01:10:28 PM INFO: Cell returned
06/20/2018 01:10:28 PM INFO: Running cell:
# RUN CELL 5
from keras.optimizers import SGD
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
batch_size = 20

06/20/2018 01:10:28 PM INFO: Cell returned
06/20/2018 01:10:28 PM INFO: Running cell:
train_gen = DataSequence(train_file_list, batch_size, augment=True)
validate_gen = DataSequence(val_file_list, batch_size, augment=True)

# use multigpu 8
# model.compile(optimizer=Adam(lr=1e-4), loss='categorical_crossentropy', metrics = ['accuracy'])
# parallel_model.compile(optimizer=Adam(lr=1e-4), loss='categorical_crossentropy', metrics = ['accuracy'])

model.compile(loss='categorical_crossentropy', optimizer=SGD(lr=0.003, momentum=0.9, nesterov=True), metrics=['accuracy'])
experiment_id = get_experiment_id()

callbacks = [
    EarlyStopping(monitor='val_loss', patience=10, verbose=1),
    ModelCheckpoint("inceptionV3_sgd"+".hdf5", monitor='val_loss', verbose=1, save_best_only=True, mode='auto'),
    ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=10, verbose=1, mode='auto', epsilon=0.01, cooldown=0, min_lr=1e-6)
]

history = model.fit_generator(generator=train_gen, 
                              validation_data=validate_gen,
                              steps_per_epoch=(len(train_gen)),
                              validation_steps=len(validate_gen),
                              verbose=1,
                              epochs=50,
                              callbacks=callbacks,
                              workers=12, # recommend: number of cpu cores
                              use_multiprocessing=True)

06/20/2018 05:49:02 PM INFO: Cell returned
Traceback (most recent call last):
  File "/anaconda/envs/py35/bin/runipy", line 11, in <module>
    sys.exit(main())
  File "/anaconda/envs/py35/lib/python3.5/site-packages/runipy/main.py", line 158, in main
    nb_runner.run_notebook(skip_exceptions=args.skip_exceptions)
  File "/anaconda/envs/py35/lib/python3.5/site-packages/runipy/notebook_runner.py", line 232, in run_notebook
    self.run_cell(cell)
  File "/anaconda/envs/py35/lib/python3.5/site-packages/runipy/notebook_runner.py", line 151, in run_cell
    msg = self.kc.get_iopub_msg(timeout=1)
  File "/anaconda/envs/py35/lib/python3.5/site-packages/jupyter_client/client.py", line 81, in get_iopub_msg
    return self.iopub_channel.get_msg(*args, **kwargs)
  File "/anaconda/envs/py35/lib/python3.5/site-packages/jupyter_client/blocking/channels.py", line 57, in get_msg
    raise Empty
queue.Empty
[01;32mbintang@bintang-gpu-2[00m:[01;34m~/notebooks/bintang-notebook/tugas-akhir/notebook-pydata[00m$ [IPKernelApp] WARNING | Parent appears to have exited, shutting down.
exit
exit

Script done on Wed Jun 20 18:10:35 2018
